<?php

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');


$animal = new Animal("shaun");

echo "Name : " .$animal->name . "<br>"; // "shaun"
echo "Legs : " .$animal->legs . "<br>"; // 4
echo "Cold Blooded : " .$animal->cold_blooded . "<br>". "<br>"; // "no"

$kodok = new Frog("Buduk");
echo "Name : " .$kodok->name . "<br>"; // "shaun"
echo "Legs : " .$kodok->legs . "<br>";
echo "Cold Blooded : " .$kodok->cold_blooded . "<br>"; // "no"
echo $kodok->jump("Jump : "). "<br>". "<br>";

$ape = new Ape("Kera Sakti");
echo "Name : " .$ape->name . "<br>"; // "shaun"
echo "Legs : " .$ape->legs . "<br>";
echo "Cold Blooded : " .$ape->cold_blooded . "<br>"; // "no"
echo $ape->yell("Yell : "). "<br>";
?>
